from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render
import json
from graphcoloring.minisolvers import MinisatSolver

@csrf_exempt
def graph(request):
    if request.method == 'GET':
        return render(request, 'graph.html')
    elif request.method == 'POST':
        adj = request.POST['adj_data']
        adj_list = json.loads(adj)
        color = {}
        cur_color = 0
        colored_with_cur = {}
        uncolored = sorted(adj_list.items(), key=lambda k: len(k[1]), reverse=True)
        while len(uncolored) > 0:
            first = int(uncolored[0][0])
            del uncolored[0]
            color[first] = cur_color
            colored_with_cur[cur_color] = []
            colored_with_cur[cur_color].append(first)
            for v in uncolored[:]:
                if len(set(v[1]) & set(colored_with_cur[cur_color])) < 1:
                    del uncolored[uncolored.index(v)]
                    cur_vertex = int(v[0])
                    color[cur_vertex] = cur_color
                    colored_with_cur[cur_color].append(cur_vertex)
            cur_color += 1
        return HttpResponse(json.dumps(color))
       
        
         

# def graph(request):
#     if request.method == 'GET':
#         return render(request, 'graph.html')
#     elif request.method == 'POST':
#         adj = request.POST['adj_data']
#         adj_list = json.loads(adj)
#         color = {}
#         uncolored = sorted(adj_list.items(), key=lambda k: len(k[0]), reverse=True)
#         print(uncolored)

#         S = MinisatSolver()

#         for a in range(1,3*len(uncolored)+1):
#             S.new_var()
        
#         # At least one color true for each node
#         var_number = 1
#         for i in uncolored:
#             S.add_clause([var_number, var_number+1, var_number+2])
#             S.add_clause([-1*var_number, -1*(var_number+1)])
#             S.add_clause([-1*var_number, -1*(var_number+2)])
#             S.add_clause([-1*(var_number+1), -1*(var_number+2)])
#             var_number = var_number+3

#         # Nodes that share the an edge cant share the same color
#         for adj in uncolored:
#             cur_node = 1 + 3*(int(adj[0])-1)
#             adj_node = adj[1]
#             for i in range(0,len(adj_node)):
#                 cur_adj_node = 1 + 3*((adj_node[i])-1)
#                 S.add_clause([-1*cur_node, -1*cur_adj_node])
#                 S.add_clause([-1*(cur_node+1), -1*(cur_adj_node+1)])
#                 S.add_clause([-1*(cur_node+2), -1*(cur_adj_node+2)])

#         # Find solution
#         success = S.solve()
#         if success:
#             result = list(S.get_model())
#             print(result)
#         else:
#             result = []
        
#         # Separate "true" variables
#         colored = []
#         for index in range(0,len(result)):
#             if result[index] == 1:
#                 colored.append(index+1)
        
#         # Colorify each "true" variables
#         for node in colored:
#             if node%3 == 0:
#                 colored_node_id = node//3
#             else:
#                 colored_node_id = (node//3) +1

#             colored_node_color = node%3
#             if colored_node_color == 1:
#                 color[colored_node_id] = 0
#             elif colored_node_color == 2:
#                 color[colored_node_id] = 1
#             else:
#                 color[colored_node_id] = 2

#         return HttpResponse(json.dumps(color))
        

       
    
        
         
           