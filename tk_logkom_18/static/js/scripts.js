var board = document.getElementById("myCanvas");
var ctx = board.getContext("2d");
var node_set = [];
var adj_list = {};
var node_id = 0;
var tool = "vertex";
var edge_started = false;
var colorset = ['#698B22', '#CD2626', '#00FFFF'];

$(document).ready(function(){
  $("#vertex").click(function(){  
    resetcolor();
  
      tool = "vertex";
      $("#vertex").css("background-color", "3A9EFD");
  });
  $("#edge").click(function(){
    resetcolor();
    tool = "edge";
      $("#edge").css("background-color", "3A9EFD");

  });
  $("#clear").click(function(){
    resetcolor();
    clears();
      $("#clear").css("background-color", "3A9EFD");
  });
});

function resetcolor(){
  $("#vertex").css("background-color", "3E4491");
  $("#edge").css("background-color", "3E4491");
  $("#clear").css("background-color", "3E4491");

}

board.onmousedown = function(e) {
    var x = e.pageX - this.offsetLeft;
    var y = e.pageY - this.offsetTop;
    if (tool=="vertex"){
        ctx.fillStyle = "#000000"
        ctx.beginPath();
        ctx.arc(x, y, 10, 0 , 2 * Math.PI, false);
        ctx.fill();
        ctx.closePath(); 
        ctx.fillText(++node_id, x+10, y-10);
        node_set.push({id: node_id, "x": x, "y": y});
    }
    if (tool=="edge"){
        for (var i=0;i<node_set.length;i++){
            var n_x = node_set[i].x;
            var n_y = node_set[i].y;
            // if there is only one node
            if (!adj_list[i+1]){
                adj_list[i+1] = [];
            }

            if (x <= n_x+10 && x >= n_x-10 && y <= n_y+10 && y >= n_y-10){
                if (!edge_started){
                    s_x = n_x, s_y = n_y;
                    glitter(s_x, s_y);
                    ctx.beginPath();
                    ctx.moveTo(n_x, n_y);
                    edge_started = true;
                    bgn_vertex = i+1;
                }
                else {
                    draw_edge(n_x, n_y);
                    glitter(s_x, s_y);
                    edge_started = false;
                    adj_list[bgn_vertex].push(node_set[i]["id"]);
                    adj_list[node_set[i].id].push(node_set[bgn_vertex-1]["id"]);
                }      
            }
        }
    }
} 

function glitter (x, y){
    if (!edge_started){
        ctx.fillStyle = "#aaaaaa"
    }
    else {
        ctx.fillStyle = "#000000"
    }
    ctx.beginPath();
    ctx.moveTo(x, y);
    ctx.arc(x, y, 10, 0 , 2 * Math.PI, false);
    ctx.fill();
    ctx.closePath();
}

function draw_edge (e_x, e_y){
    ctx.lineTo(e_x, e_y);
   ctx.stroke();
   ctx.closePath();
}

function send(){
    $.post("/", {adj_data: JSON.stringify(adj_list)}, function(data, status) {
        colorify_graph(JSON.parse(data));
  });
}

function colorify_graph (data){
    for (var i=0; i<node_set.length; i++){
        ctx.fillStyle = colorset[data[i+1]];
        ctx.beginPath();
        ctx.arc(node_set[i].x, node_set[i].y, 10, 0 , 2 * Math.PI, false);
        ctx.fill();
        ctx.closePath();  
    }
}
// function colorify_graph (data){
//     var colors = colorset;
//     console.log(data.length) // ???????????? size or len
//     for (var i = 0; i<node_set.length; i++) {
//       if (typeof colorset[data[i+1]] === 'undefined' && data.length != 0 || 
//         typeof colorset[data[i+1]] === 'undefined' && typeof data === 'undefined') {
//         colors = ['#000000', '#000000', '#000000'];
//         setTimeout(4000);
//         alert("Graph that you created not satisfiable. Please draw again!");
//         window.location.href = "/";
//         break;
//       }
//     }
//     // apa ini benar kalo blom ada edge
//     if (data.length == 0){
//         console.log("no edge");
//         ctx.fillStyle = colors[Math.floor(Math.random() * colors.length)];
//         ctx.beginPath();
//         ctx.arc(node_set[i].x, node_set[i].y, 10, 0 , 2 * Math.PI, false);
//         ctx.fill();
//         ctx.closePath();       
//     }

//     // ini udah yg bisa kalo unsatisfi
//     for (var i=0; i<node_set.length; i++){
//         console.log(data);
//         console.log(colors[data[i+1]]);
//         ctx.fillStyle = colors[data[i+1]];
//         ctx.beginPath();
//         ctx.arc(node_set[i].x, node_set[i].y, 10, 0 , 2 * Math.PI, false);
//         ctx.fill();
//         ctx.closePath();  
//         // ctx.fillStyle = colorset[Math.floor(Math.random() * colorset.length)];
//         // items[Math.floor(Math.random() * items.length)];
//     }
// }

function clears(){
  document.location.href="/";   
}

